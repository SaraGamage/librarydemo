﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyLibrary.Models
{
    public class Borrower
    {
        /// <summary>
        ///  Borrower id as a guid
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the borrower first name
        /// </summary>
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last first name
        /// </summary>
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
    }
}