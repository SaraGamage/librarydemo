﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLibrary.Models
{
    public class BorrowedBook
    {
        public Guid BorrowerId { get; set; }

        public Guid BookId { get; set; }

        public DateTime DateBorrowed { get; set; }

        public DateTime DueDate { get; set; }
    }

    public class BorrowedBookViewModel
    {
        public Borrower Borrower { get; set; }

        public BookViewModel Book { get; set; }

        [Display(Name = "Date Borrowed")]
        public DateTime DateBorrowed { get; set; }
        
        [Display(Name = "Due Date")]
        public DateTime DueDate { get; set; }
    }
}