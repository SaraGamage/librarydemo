﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyLibrary.Models
{
    public class Book
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }
    }

    public class BookViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Title of the book")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Authors full name")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Available to borrow")]
        public bool Available { get; set; }
    }

    public class BorrowBookViewModel
    {
        public BorrowBookViewModel()
        {
            Borrowers = new List<Borrower>();
        }

        public BookViewModel Book { get; set; }

        public Borrower Borrower { get; set; }

        public List<Borrower> Borrowers { get; set; } 

        public string SearchResultInfo { get; set; }
    }

    public class BookBorrowResult
    {
        public DateTime DueDate { get; set; }

        public string Message { get; set; }
    }
}