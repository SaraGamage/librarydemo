﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MyLibrary.Models
{
    public class BookSearchViewModel
    {
        public BookSearchViewModel()
        {
            Books = new List<BookViewModel>();
        }

        [Display(Name = "Title of the book")]
        public string Title { get; set; }

        [Display(Name = "Authors full name")]
        public string Author { get; set; }

        [Display(Name = "Available to borrow")]
        public bool Available { get; set; }

        /// <summary>
        /// Search results Books
        /// </summary>
        public IEnumerable<BookViewModel> Books { get; set; }

        public string SearchResultInfo { get; set; }
    }
}