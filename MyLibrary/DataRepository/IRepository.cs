﻿using System;
using System.Collections.Generic;
using MyLibrary.Models;

namespace MyLibrary.DataRepository
{
    public interface IRepository
    {
        void AddBook(BookViewModel book);
        BookViewModel GetBook(Guid id);
        IEnumerable<BookViewModel> GetBooks(string title, string author);
        IEnumerable<BookViewModel> GetBooks();

        void AddBorrower(Borrower model);
        IEnumerable<Borrower> GetBorrowers();
        IEnumerable<Borrower> GetBorrowers(string firstName, string lastName);
        IEnumerable<BorrowedBookViewModel> GetOverdueBooks();

        BookBorrowResult BorrowBook(Guid bookId, Guid borrowerId);
    }
}
