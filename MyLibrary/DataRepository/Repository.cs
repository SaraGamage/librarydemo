﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Ajax.Utilities;
using MyLibrary.Models;
using MyLibrary.Common;

namespace MyLibrary.DataRepository
{
    public class Repository : IRepository
    {
        public void AddBook(BookViewModel book)
        {
            MvcApplication.BookDataStore.Add(new Book
            {
                // NOTE: can use AutoMapper to help here
                Id = book.Id,
                Author = book.Author,
                Title = book.Title
            });
        }

        private bool BookIsAvailable(Guid bookId)
        {
            return MvcApplication.BorrowedBooksDataStore.All(borrowEntry => borrowEntry.BookId != bookId);
        }

        public BookViewModel GetBook(Guid id)
        {
            var book = MvcApplication.BookDataStore.SingleOrDefault(item => item.Id.Equals(id));

            if(book == null)
                return new BookViewModel();

            return BookToViewModel(book);
        }

        // TODO: paging with skip and take
        public IEnumerable<BookViewModel> GetBooks(/*int skip, int take*/)
        {
            return MvcApplication.BookDataStore.Select(BookToViewModel);
        }

        public IEnumerable<BookViewModel> GetBooks(string title, string author)
        {
            Expression<Func<Book, bool>> authorSearch = book => book.Author.ContainsIgnoreCase(author);

            Expression<Func<Book, bool>> titleSearch = book => book.Title.ContainsIgnoreCase(title);
            
            var titleSupplied = !title.IsNullOrWhiteSpace();
            var authorSupplied = !author.IsNullOrWhiteSpace();

            if (titleSupplied && authorSupplied)
            {
                return SearchBooks(titleSearch.And(authorSearch));
            }
            
            if (authorSupplied)
            {
                return SearchBooks(authorSearch);
            }
            
            if (titleSupplied)
            {
                return SearchBooks(titleSearch);
            }

            return new List<BookViewModel>();
        }

        private IEnumerable<BookViewModel> SearchBooks(Expression<Func<Book, bool>> queryExpression)
        {
            if (queryExpression == null)
                return new List<BookViewModel>();

            var result = MvcApplication.BookDataStore.Where(queryExpression.Compile()).ToList();

            return result.Select(BookToViewModel);
        }

        public void AddBorrower(Borrower borrower)
        {
            MvcApplication.BorrowerDataStore.Add(borrower);
        }

        public IEnumerable<Borrower> GetBorrowers()
        {
            return MvcApplication.BorrowerDataStore;
        }

        public IEnumerable<Borrower> GetBorrowers(string firstName, string lastName)
        {
            Expression<Func<Borrower, bool>> firstNameSearch = borrower => borrower.FirstName.ContainsIgnoreCase(firstName);

            Expression<Func<Borrower, bool>> lastNameSearch = borrower => borrower.LastName.ContainsIgnoreCase(lastName);

            var firstNameSupplied = !firstName.IsNullOrWhiteSpace();
            var lastNameSupplied = !lastName.IsNullOrWhiteSpace();

            if (firstNameSupplied && lastNameSupplied)
            {
                return SearchBorrowers(firstNameSearch.And(lastNameSearch));
            }

            if (lastNameSupplied)
            {
                return SearchBorrowers(lastNameSearch);
            }

            if (firstNameSupplied)
            {
                return SearchBorrowers(firstNameSearch);
            }

            return new List<Borrower>();
        }

        private IEnumerable<Borrower> SearchBorrowers(Expression<Func<Borrower, bool>> queryExpression)
        {
            if (queryExpression == null)
                return new List<Borrower>();

            var result = MvcApplication.BorrowerDataStore.Where(queryExpression.Compile()).ToList();

            return result;
        }

        public IEnumerable<BorrowedBookViewModel> GetOverdueBooks()
        {
            // TODO: overdue logic needed
            var borrowedBooks = MvcApplication.BorrowedBooksDataStore
                .Where(item => item.DateBorrowed <= DateTime.Today.AddDays(-7))
                .ToList();

            return MapBorrowedBookToViewModel(borrowedBooks);
        }

        public BookBorrowResult BorrowBook(Guid bookId, Guid borrowerId)
        {
            var borrowEntry = MvcApplication.BorrowedBooksDataStore
                .SingleOrDefault(entry => entry.BookId == bookId);

            // likely someone else borrowed the book before this borrower clicked 'check out'
            // this seems silly though, because it's a physical item in the real world in a real
            // library a borrower brings the book to the counter
            if (borrowEntry != null)
            {
                return new BookBorrowResult
                {
                    Message =
                        "You were unable to borrow this book, someone else used this application to borrow it before you."
                };
            }

            var successEntry = new BorrowedBook
            {
                BookId = bookId,
                BorrowerId = borrowerId,
                DateBorrowed = DateTime.Now,
                DueDate = DateTime.Now.AddDays(21)
            };

            MvcApplication.BorrowedBooksDataStore.Add(successEntry);

            return new BookBorrowResult
                {
                    Message = string.Format("You have sucessfully borrowed that book, it is due back on {0:ddd d MMM yyyy}", successEntry.DueDate)
                };
        }

        public bool ReturnBook(Guid bookId, Guid borrowerId)
        {
            // TODO: plan is to remove the entry from the BorrowedBooksDataStore
            // If there's a need to log past borrows (example to prevent borrower always borrowing same book all year)
            // that data can go in another datastore
            
            throw new NotImplementedException("not part of requirements");
        }

        private IEnumerable<BorrowedBookViewModel> MapBorrowedBookToViewModel(IEnumerable<BorrowedBook> borrowedBooks)
        {
            // An ORM and something like AutoMapper would have taken care of this for us, helping find and map the data
            var results = borrowedBooks.Select(borrowedBook => new BorrowedBookViewModel
            {
                Book = BookToViewModel(MvcApplication.BookDataStore.SingleOrDefault(book => book.Id == borrowedBook.BookId)),
                Borrower = MvcApplication.BorrowerDataStore.SingleOrDefault(borrower => borrower.Id == borrowedBook.BorrowerId),
                DateBorrowed = borrowedBook.DateBorrowed,
                DueDate = borrowedBook.DueDate
            }).ToList();

            return results;
        }

        private BookViewModel BookToViewModel(Book book)
        {
            return new BookViewModel
            {
                Id = book.Id,
                Title = book.Title,
                Author = book.Author,
                Available = BookIsAvailable(book.Id)
            };
        }
    }
}