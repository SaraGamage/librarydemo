﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MyLibrary.Models;

namespace MyLibrary
{
    public class MvcApplication : HttpApplication
    {
        public static List<Book> BookDataStore { get; set; }

        public static List<Borrower> BorrowerDataStore { get; set; }

        public static List<BorrowedBook> BorrowedBooksDataStore { get; set; }

        protected void Application_Start()
        {
            BookDataStore = InMemoryLibrary.Books();
            BorrowerDataStore = InMemoryLibrary.LibraryMembers();

            BorrowedBooksDataStore = InMemoryLibrary.BorrowedBooks(BookDataStore, BorrowerDataStore);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}