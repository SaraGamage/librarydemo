﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MyLibrary.DataRepository;
using MyLibrary.Models;

namespace MyLibrary.Controllers
{
    /// <summary>
    /// This class is used to create and view books
    /// </summary>
    public class BookController : Controller
    {
        private readonly IRepository _repository;

        public BookController(IRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// This Create action creates an empty view to add a book
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public virtual ActionResult Create()
        {
            return View(new BookViewModel());
        }

        /// <summary>
        /// When create button click this Create action gets the BookViewModel with values
        /// If all the conditions are met the book get added and redirect to Create action
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public virtual ActionResult Create(BookViewModel model)
        {
            if (!ModelState.IsValid)
               return View(model);

            model.Id = Guid.NewGuid();
            _repository.AddBook(model);

           return RedirectToAction("Create");
        }

        /// <summary>
        /// This ViewBooks action return a view with list of added books
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public virtual ActionResult ViewBooks()
        {
            return View(_repository.GetBooks());
        }

        [HttpGet]
        public virtual ActionResult Search()
        {
            return View(new BookSearchViewModel());
        }

        [HttpPost]
        public virtual ActionResult Search(BookSearchViewModel model)
        {
            model.Books = _repository.GetBooks(model.Title, model.Author);

            if (!model.Books.Any())
            {
                model.SearchResultInfo = "No matching books were found, please adjust your search and try again.";
            }

            return View(model);
        }
    }
}
