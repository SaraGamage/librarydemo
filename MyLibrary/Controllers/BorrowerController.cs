﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MyLibrary.DataRepository;
using MyLibrary.Models;

namespace MyLibrary.Controllers
{
    /// <summary>
    /// This class is used to create and view borrowers
    /// </summary>
    public class BorrowerController : Controller
    {
        private readonly IRepository _repository;

        public BorrowerController(IRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// This Create action creates an empty view to add a borrower
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public virtual ActionResult Create()
        {
            return View(new Borrower());
        }

        /// <summary>
        /// When create button click this Create action gets the BorrowerViewModel with values
        /// If all the conditions are met the Borrower get added and redirect to Create action
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public virtual ActionResult Create(Borrower model)
        {
           if (!ModelState.IsValid)
               return View(model);

           model.Id = Guid.NewGuid();
           _repository.AddBorrower(model);

           return RedirectToAction("Create");
        }

        /// <summary>
        /// This ViewBooks action return a view with list of added books
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public virtual ActionResult ViewBorrowers()
        {
            return View(_repository.GetBorrowers());
        }

        [HttpGet]
        public virtual ActionResult OverdueBooks()
        {
            var overdueBooks = _repository.GetOverdueBooks();
            return View(overdueBooks);
        }

        [HttpGet]
        public virtual ActionResult BorrowBook(Guid bookId)
        {
            var book = _repository.GetBook(bookId);

            return View(new BorrowBookViewModel
            {
                Book = book
            });
        }

        [HttpPost]
        public virtual ActionResult BorrowBook(BorrowBookViewModel model)
        {
            // choosing to use the query string here to re-load the book data, 
            // another option was to have the book id as hidden field in the form
            // even better solution is to evolve the application to always be in the context of a real libray member, i.e. when they scan in
            // the last option would make borrowing quicker, it would be about finding a book (again scanning a barcode).
            model.Book = _repository.GetBook(Guid.Parse(Request.QueryString["bookId"]));

            model.Borrowers = _repository.GetBorrowers(model.Borrower.FirstName, model.Borrower.LastName).ToList();

            if (!model.Borrowers.Any())
            {
                model.SearchResultInfo = "No matching borrowers were found, please adjust your search and try again.";
            }

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult CheckoutBook(Guid bookId, Guid borrowerId)
        {
            var result = _repository.BorrowBook(bookId, borrowerId);

            return View(result);
        }
    }
}
