﻿using System;
using System.Web.Caching;

namespace MyLibrary.Extensions
{
    /// <summary>
    /// The cache extensions.
    /// </summary>
    public static class CacheExtensions
    {
        /// <summary>
        /// The get or store function that sets items into cache if they do not exist already and returns them.
        /// </summary>
        /// <param name="cache">
        /// The cache.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="generator">
        /// The generator.
        /// </param>
        /// <param name="expirationSeconds">
        /// The expiration seconds.
        /// </param>
        /// <typeparam name="T">
        /// The type of object to save
        /// </typeparam>
        /// <returns>
        /// The T.
        /// </returns>
        public static T GetOrStore<T>(this Cache cache, string key, Func<T> generator, int expirationSeconds = 3600)
        {
            object result = cache[key];
            if (result == null)
            {
                result = generator();
                cache.Add(
                    key,
                    result,
                    null,
                    DateTime.Now.AddSeconds(expirationSeconds),
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.High,
                    null);
            }

            return (T)result;
        }

    }
}