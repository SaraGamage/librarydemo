﻿using System;

namespace MyLibrary.DataAnnotations
{
    /// <summary>
    /// Display a radio or checkbox list inline.
    /// </summary>
    public class SplitListIntoTwoColumnsAttribute : Attribute
    {
    }
}
