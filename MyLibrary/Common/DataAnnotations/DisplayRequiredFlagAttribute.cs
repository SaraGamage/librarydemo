﻿using System;

namespace MyLibrary.DataAnnotations
{
    /// <summary>
    /// The display required flag attribute.
    /// </summary>
    public class DisplayRequiredFlagAttribute : Attribute
    {
    }
}
