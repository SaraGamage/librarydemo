﻿using System;

namespace MyLibrary.Common
{
    public static class StringHelpers
    {
        public static bool ContainsIgnoreCase(this string source, string toCheck)
        {
            return source.Contains(toCheck, StringComparison.OrdinalIgnoreCase);
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            if (string.IsNullOrWhiteSpace(toCheck))
                return false;

            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}