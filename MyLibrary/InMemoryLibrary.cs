using System;
using System.Collections.Generic;
using System.Linq;
using MyLibrary.Models;

namespace MyLibrary
{
    public class InMemoryLibrary
    {
        public static List<Book> Books()
        {
            return new List<Book>
            {
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "Leading Snowflakes",
                    Author = "Oren Ellenbogen"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "Programming F#",
                    Author = "Chris Smith"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "JavaScript Patterns",
                    Author = "Stoyan Stefanov"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "DARK GRAY MOUNTAIN",
                    Author = "John Grisham"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "GONE GIRL",
                    Author = "Gillian Flynn"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "LEAVING TIME",
                    Author = "Jodi Picoult"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "BURN",
                    Author = "James Patterson and Michael Ledwidge"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "DARK PLACES",
                    Author = "Gillian Flynn"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "DEADLINE",
                    Author = "John Sandford"
                },
                new Book
                {
                    Id = Guid.NewGuid(),
                    Title = "Leading Sheep",
                    Author = "Stoyan Stefanov"
                },
            };
        }

        public static List<Borrower> LibraryMembers()
        {
            return new List<Borrower>
            {
                new Borrower
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Aaron",
                    LastName = "Hank"
                },

                new Borrower
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Barry",
                    LastName = "Marion"
                },

                new Borrower
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Dean",
                    LastName = "Howard"
                },
            };
        }

        /// <summary>
        /// Requires a copy of the books and members, so it can set up existing borrowed books
        /// </summary>
        public static List<BorrowedBook> BorrowedBooks(List<Book> bookDataStore, List<Borrower> borrowerDataStore)
        {
            // borrow term 21 days (3 weeks)
            return new List<BorrowedBook>
            {
                new BorrowedBook
                {
                    BorrowerId = borrowerDataStore.Single(member => member.LastName == "Howard").Id,
                    BookId = bookDataStore.Single(book => book.Title == "Leading Snowflakes").Id,
                    DateBorrowed = DateTime.Today.AddDays(-15),
                    DueDate = DateTime.Today.AddDays(-15+21)
                },
                new BorrowedBook
                {
                    BorrowerId = borrowerDataStore.Single(member => member.LastName == "Marion").Id,
                    BookId = bookDataStore.Single(book => book.Title == "Programming F#").Id,
                    DateBorrowed = DateTime.Today.AddDays(-17),
                    DueDate = DateTime.Today.AddDays(-17+21)
                },
                new BorrowedBook
                {
                    BorrowerId = borrowerDataStore.Single(member => member.LastName == "Hank").Id,
                    BookId = bookDataStore.Single(book => book.Title == "Leading Snowflakes").Id,
                    DateBorrowed = DateTime.Today.AddDays(-2),
                    DueDate = DateTime.Today.AddDays(-2+21)
                }
            };
        }
    }
}